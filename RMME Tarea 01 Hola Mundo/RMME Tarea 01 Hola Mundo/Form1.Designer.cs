﻿namespace RMME_Tarea_01_Hola_Mundo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btbienve = new System.Windows.Forms.Button();
            this.btbye = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblbienve = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.0676F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.9324F));
            this.tableLayoutPanel1.Controls.Add(this.btbienve, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btbye, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(83, 317);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.421053F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.57895F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(683, 95);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btbienve
            // 
            this.btbienve.BackColor = System.Drawing.Color.Navy;
            this.btbienve.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btbienve.BackgroundImage")));
            this.btbienve.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btbienve.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btbienve.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lime;
            this.btbienve.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Yellow;
            this.btbienve.Font = new System.Drawing.Font("Perpetua Titling MT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbienve.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btbienve.Location = new System.Drawing.Point(3, 11);
            this.btbienve.Name = "btbienve";
            this.btbienve.Size = new System.Drawing.Size(329, 81);
            this.btbienve.TabIndex = 0;
            this.btbienve.Text = "Bienvenido ";
            this.btbienve.UseVisualStyleBackColor = false;
            this.btbienve.Click += new System.EventHandler(this.button1_Click);
            // 
            // btbye
            // 
            this.btbye.BackColor = System.Drawing.Color.Navy;
            this.btbye.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btbye.BackgroundImage")));
            this.btbye.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btbye.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lime;
            this.btbye.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Yellow;
            this.btbye.Font = new System.Drawing.Font("Perpetua Titling MT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbye.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btbye.Location = new System.Drawing.Point(338, 11);
            this.btbye.Name = "btbye";
            this.btbye.Size = new System.Drawing.Size(329, 81);
            this.btbye.TabIndex = 1;
            this.btbye.Text = "Hasta luego ";
            this.btbye.UseVisualStyleBackColor = false;
            this.btbye.Click += new System.EventHandler(this.btbye_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lblbienve, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(86, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(677, 299);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lblbienve
            // 
            this.lblbienve.BackColor = System.Drawing.Color.Transparent;
            this.lblbienve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblbienve.Font = new System.Drawing.Font("Engravers MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbienve.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblbienve.Location = new System.Drawing.Point(3, 0);
            this.lblbienve.Name = "lblbienve";
            this.lblbienve.Size = new System.Drawing.Size(671, 299);
            this.lblbienve.TabIndex = 1;
            this.lblbienve.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(858, 450);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Saludo Hyliano ";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btbienve;
        private System.Windows.Forms.Button btbye;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblbienve;
        private System.Windows.Forms.Timer timer1;
    }
}

